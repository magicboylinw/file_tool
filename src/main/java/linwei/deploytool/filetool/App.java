package linwei.deploytool.filetool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;

import org.apache.commons.lang.StringUtils;

import com.google.common.io.Files;

/**
 * Hello world!
 *
 */
public class App implements Runnable {

	public void run() {
		try {
			/* 抽取文件的源目录 */
			String src = System.getProperty("src");
			/* 目标目录 */
			String target = System.getProperty("target");
			/* 文件列表 */
			String config = System.getProperty("config");
			/* 输出结果 */
			String output = System.getProperty("output");
			File outputFile = new File(output);
			if(!outputFile.exists()){
				outputFile.createNewFile();
			}
			Writer outputWriter = new FileWriter(outputFile);
			
			/* 读取config，所有要抽取的文件，#开头为注释  */
			BufferedReader input = new BufferedReader(new FileReader(new File(config)));
			String line = null;
			while((line = input.readLine()) != null){
				if((line = StringUtils.trimToNull(line)) == null){
					continue;
				}
				
				try {
					/* 排除注释 */
					if(line.startsWith("#")){
						continue;
					}
					/* 统一分隔符 */
					line = StringUtils.replace(line, "/", File.separator);
					line = StringUtils.replace(line, "\\", File.separator);
					/* 开头添加斜杠 */
					if(!line.startsWith(File.separator)){
						line = File.separator + line;
					}
					/* java -> class */
					if(line.endsWith(".java")){
						line = line.substring(0, line.length() - 5);
						line += ".class";
					}
					/* 排除开头的 supply_v2 路径 */
					line = StringUtils.replace(line, File.separator + "supply_v2", "", 1);
					
					File srcFile = null;
					File targetFile = null;
					/* src下的文件，复制到WebRoot/WEB-INF/classes下 */
					if(line.startsWith(File.separator + "src")){
						line = line.substring(4);
						targetFile = new File(
								target + File.separator + "WEB-INF" + File.separator + "classes" + line);
						srcFile = new File(
								src + File.separator + "WebRoot" + File.separator + "WEB-INF" + File.separator + "classes" + line);
					
					/* resources下的文件，复制到WebRoot/WEB-INF/classes下 */
					} else if(line.startsWith(File.separator + "resources")) {
						line = line.substring(10);
						targetFile = new File(
								target + File.separator + "WEB-INF" + File.separator + "classes" + line);
						srcFile = new File(
								src + File.separator + "WebRoot" + File.separator + "WEB-INF" + File.separator + "classes" + line);
					
					/* 否则，就是WebRoot下的文件，删除路径中的WebRoot */
					} else {
						srcFile = new File(src + line);
						targetFile = new File(target + line.substring(8));
					}
					
					/* 进行复制 */
					if(!srcFile.exists() || !srcFile.canRead()){
						throw new Exception();
					}
					Files.createParentDirs(targetFile);
					Files.copy(srcFile, targetFile);
					
					/* 查找内部类 */
					if (srcFile.getName().endsWith("class")) {
						String fileName = Files.getNameWithoutExtension(srcFile.getCanonicalPath());
						for (File f : srcFile.getParentFile().listFiles()) {
							if (f.isFile() && f.getName().endsWith(".class") && f.getName().startsWith(fileName + "$")) {
								File copyTo = new File(StringUtils.replace(
										f.getCanonicalPath(), src + File.separator + "WebRoot", target, 1));
								Files.createParentDirs(copyTo);
								Files.copy(f, copyTo);
							}
						}
					}
				} catch (Exception e) {
					outputWriter.write(line + "复制失败");
					outputWriter.flush();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		App app = new App();
		app.run();
	}
}
